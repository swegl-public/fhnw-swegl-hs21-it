package library;

public class Book {
    private boolean borrowed;

    public boolean isBorrowed() {
        return borrowed;
    }

    public boolean isAvailable() {
        return !isBorrowed();
    }

    public long getReturnDate() {
        return 0;
    }

    public void borrow() {
        borrowed = true;
    }

    //TODO: find better name as refactoring
    public void returnBook() {
        borrowed = false;
    }
}
