/*
 * CREDITS: this example is based on material developed for the
 * Introduction to Programming course by Nimi Berman and Cay Horstmann
 * at San Jose State University, CA.
 * 
 * The original material can be found at:
 *    www.cs.sjsu.edu/faculty/horstman/CS46A/debugger/tutorial.html
 */
package wordanalyzer;

/**
 * A class that analyzes words.
 */
public class WordAnalyzer {

    private String word = "";

    public WordAnalyzer(String word) {
        setWord(word);
    }

    /**
     * private setter method for field word.
     * @param word    the word to be analyzed
     * @throws IllegalArgumentException if word is equal to null
     */
    private void setWord(String word) throws IllegalArgumentException {
        if (word != null) {
            this.word = word;
        } else {
            throw new IllegalArgumentException("The word needs to be non-null");
        }
    }

    /**
     * Gets the first repeated character. A character is <i>repeated</i>
     * if it occurs at least twice in adjacent positions.
     * 
     * For example, 'l' is repeated in "hollow", but 'o' is not.
     * 
     * @return the first repeated character, or 0 if none found
     */
    public char firstRepeatedCharacter() {
        for (int i = 0; i < word.length(); i++) {
            char ch = word.charAt(i);
            if (ch == word.charAt(i + 1)) {
                return ch;
            }
        }
        return 0;
    }

    /**
     * Gets the first multiply occuring character. A character is <i>multiple</i>
     * if it occurs at least twice in the word, not necessarily in adjacent positions.
     * 
     * For example, both 'o' and 'l' are multiple in "hollow", but 'h' is not.
     *
     * @return the first repeated character, or 0 if none found
     */
    public char firstMultipleCharacter() {
        for (int i = 1; i < word.length(); i++) {
            char ch = word.charAt(i);
            if (find(ch, i) >= 0) {
                return ch;
            }
        }
        return 0;
    }

    private int find(char c, int pos) {
        for (int i = pos; i < word.length(); i++) {
            if (word.charAt(i) == c) {
                return i;
            }
        }
        return -1;
    }

    /**
     * Counts the groups of repeated characters.
     *
     * For example, "mississippi!!!" has four such groups: ss, ss, pp and !!!.
     *
     * @return the number of repeated character groups
     */
    public int countRepeatedCharacters() {
        int c = 0;

        for (int i = 0; i < word.length() - 1; i++) {
            if (word.charAt(i) == word.charAt(i + 1)) { // found a repetition
                if (word.charAt(i - 1) != word.charAt(i)) // it's the start
                {
                    c++;
                }
            }
        }
        return c;
    }
}
