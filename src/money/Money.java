package money;
/*
 * Original code from Atlassion Clover: https://www.atlassian.com/software/clover
 * Some modifications made.
 */

/**
 * Money represents an amount in a certain currency.
 * The Money object itself is immutable,
 * meaning the amount and currency cannot be changed.
 * But there are methods to add and subtract the amount if the currency are the same and the changing amount is valid (not null)
 */
public class Money {
    private final int amount;
    private final String currency;

    public Money(int amount){
        this(amount, "CHF");
    }
    /**
     * Constructs a money from the given amount and currency.
     *
     * @param amount   the amount
     * @param currency the currency
     */
    public Money(int amount, String currency) {
        this.amount = amount;
        this.currency = currency;
    }

    /**
     * @return the amount of money
     */
    public int getAmount() {
        return amount;
    }

    /**
     * @return the currency of the money
     */
    public String getCurrency() {
        return currency;
    }

    /**
     * Returns a money object whose amount equals
     * amount of this object plus amount of money.
     *
     * @param money the money to be added
     * @return a money object with new amount.
     */
    public Money add(Money money) {
        return new Money(getAmount() + money.getAmount(), getCurrency());
    }

    /**
     * Returns a money object whose amount equals
     * amount of this object minus amount of money.
     *
     * @param money the money to be subtracted
     * @return a money object that holds amount of this minus amount of money
     */
    public Money subtract(Money money) {
        return add(money.negate());
    }

    /**
     * @return a money object whose amount equals
     * -amount of this object
     */
    public Money negate() {
        return new Money(-getAmount(), getCurrency());
    }

    /**
     * returns a money object whose amount equals
     * amount of this object times factor.
     *
     * @param factor the factor to multiply with
     * @return a money object that hold this amount times the given factor
     */
    public Money multiply(int factor) {
        return new Money(getAmount()*factor, getCurrency());
    }

    /**
     * @return whether the amount of this object equals zero
     */
    public boolean isZero() {
        return getAmount() == 0;
    }

    // generated methods equals & hashCode, to String
    // not to be tested
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Money)) return false;

        Money money = (Money) o;

        if (getAmount() != money.getAmount()) return false;
        return getCurrency().equals(money.getCurrency());
    }

    @Override
    public int hashCode() {
        int result = getAmount();
        result = 31 * result + getCurrency().hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "Money{" +
                "amount=" + amount +
                ", currency='" + currency + '\'' +
                '}';
    }
}
