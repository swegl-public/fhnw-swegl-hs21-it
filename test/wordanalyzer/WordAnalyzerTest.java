/*
 * CREDITS: this example is based on material developed for the
 * Introduction to Programming course by Nimi Berman and Cay Horstmann
 * at San Jose State University, CA.
 *
 * The original material can be found at:
 *    www.cs.sjsu.edu/faculty/horstman/CS46A/debugger/tutorial.html
 */
package wordanalyzer;

import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class WordAnalyzerTest {




    /**
     * Test of firstRepeatedCharacter method, of class WordAnalyzer.
     */
    @Test
    void firstRepeatedCharWithEmptyWord() {
        // given an empty word
        WordAnalyzer wa = new WordAnalyzer("");
        // when looking for the first repeated character
        final char result = wa.firstRepeatedCharacter();
        // then the result is 0
        assertEquals(0, result);
    }

    @Test
    void firstRepeatedCharAtBeginning() {
        // given the word 'aardvark'
        WordAnalyzer wa = new WordAnalyzer("aardvark");
        // then looking for the first repeated character, the result is 'a'
        assertEquals('a', wa.firstRepeatedCharacter());
    }

    @Test
    void firstRepeatedCharWithTwoRepeatedChars() {
        // given
        WordAnalyzer wa = new WordAnalyzer("roommate");
        // when, then
        assertEquals('o', wa.firstRepeatedCharacter());
    }

    @Test
    void firstRepeatedCharWithAllDifferentChars() {
        WordAnalyzer wa = new WordAnalyzer("mate");
        assertEquals(0, wa.firstRepeatedCharacter());
    }

    @Test
    void firstRepeatedCharWithOneMultipleChar() {
        WordAnalyzer wa = new WordAnalyzer("test");
        assertEquals(0, wa.firstRepeatedCharacter());
    }

    /**
     * Test of firstMultipleCharacter method, of class WordAnalyzer.
     */
    @Test
    public void testFirstMultipleCharacterWithEmptyWord() {
        WordAnalyzer wa = new WordAnalyzer("");
        assertEquals(0, wa.firstMultipleCharacter());
    }

    @Test
    public void testFirstMultipleCharacterWithNoMultipleChars() {
        WordAnalyzer wa = new WordAnalyzer("mate");
        assertEquals(0, wa.firstMultipleCharacter());
    }

    @Test
    public void testFirstMultipleCharacterWithOneMultipleChars() {
        WordAnalyzer wa = new WordAnalyzer("test");
        assertEquals('t', wa.firstMultipleCharacter());
    }

    @Test
    public void testFirstMultipleCharacterWithSeveralMultipleChars() {
        WordAnalyzer wa = new WordAnalyzer("missisippi");
        assertEquals('i', wa.firstMultipleCharacter());
    }

    /**
     * Test WordAnalyzer.countRepeatedCharacters method, of class WordAnalyzer.
     */
    @Test
    public void testCountRepeatedCharsWithEmptyWord() {
        WordAnalyzer wa = new WordAnalyzer("");
        assertEquals(0, wa.countRepeatedCharacters());
    }
    @Test
    public void testCountRepeatedCharsWithSeveralRepeatedChars() {
        WordAnalyzer wa = new WordAnalyzer("mississippi!!!");
        assertEquals(4, wa.countRepeatedCharacters());

    }
    @Test
    public void testCountRepeatedCharsWithNoRepeatedChars() {
        WordAnalyzer wa = new WordAnalyzer("test");
        assertEquals(0, wa.countRepeatedCharacters());
    }
    @Test
    public void testCountRepeatedCharsWithRepeatedCharsAtBeginning() {
        WordAnalyzer wa = new WordAnalyzer("aabbcdaaaabb");
        assertEquals(4, wa.countRepeatedCharacters());
    }

    /**
     * Test that exception is thrown
     */
    @Test
    void createWithNull(){
        // when creating a wordanalyzer with null then an IllegalArgumentException is thrown.
        assertThrows(IllegalArgumentException.class, () -> new WordAnalyzer(null));
    }
}
