package library;


import org.junit.jupiter.api.Test;

import java.sql.Date;
import java.time.Instant;
import java.util.Calendar;

import static org.junit.jupiter.api.Assertions.*;

public class BookTest {

/*
[source, ruby]
--
feature: Kurzleihe eines Buches



example: erfolgreiche Kurzleihe

given ein ausleihbares Kurzleihebuch
when ein Buch ausgeliehen wurde
then ist das Buch ist nicht mehr ausleihbar
and das Rückgabedatum ist 3 Wochen später als heute

example: nicht erfolgreiche Kurzleihe

given ein nicht ausleihbares Kurzleihebuch
when das Buch ausgeliehen werden soll
then kann das Buch nicht ausgeliehen werden
and das Rückgabedatum bleibt unverändert.
--

 */
    /*
    Livecoding:
    Exclude the Datehandling for now, too complicated.
    Create Book class and methods all with default instantiation.
    Test fails, because isBorrowable is false
    Use a long for the date.
    Make it green by implementing the logik isAvailable === !isBorrowed.
    Refactor: Nothing right now
    Possible Questions: don't we need to implement isBorrowed correctly?
    We do it with the next test
     */
    @Test
    void testANewBookIsAvailable(){
        //given ein Buch
        Book book = new Book(); // live coding: create Book class in package library.
        // then ist das Buch nicht ausgeliehen
        assertFalse(book.isBorrowed()); //Live coding: create Method, import static method
        //then ist der Status auf ausleihbar
        assertTrue(book.isAvailable()); //Live coding: create Method, import static method
    }

    /* livecoding
    Erst mal ohne Dateprüfung, es ist einfacher
    Run: false!
    Implement borrow - field borrowed = true.
    Implement isBorrowed: return borrowed.
    Was fällt uns auf - wir brauchen noch eine Methode return, die borrowed wieder zurücksetzt.
    Wir implementieren das aber nicht, sondern schreiben es auf eine Liste.

     */
    @Test
    void borrowANewBook(){
        //given ein neues Buch
        Book book = new Book();
        // when es ausgeliehen wird
        book.borrow();
        // then ist der Status ausgeliehen
        assertTrue(book.isBorrowed());
        // and das Buch ist nicht verfügbar
        assertFalse(book.isAvailable());
        // and das RückgabeDatum ist 21 Tage später als heute
    }

    @Test
    void returnBorrowedBook(){
        // given a borrowed book
        Book book = new Book();
        book.borrow();
        // when returning the book
        book.returnBook();
        // then the book is not borrowed anymore
        // and the book is available.
        assertFalse(book.isBorrowed());
        assertTrue(book.isAvailable());
    }
}
